function showSplash()
	splash = display.newImage( "Resources/images/splash.png" , display.contentWidth*0.5, display.contentHeight*0.5)
	splash.alpha = 0
	timer.performWithDelay( 1000, function () transition.fadeIn( splash, { time=1000 } ) end ) 
	timer.performWithDelay( 2500, function () transition.fadeOut( splash, { time=1000 } ) end ) 
end