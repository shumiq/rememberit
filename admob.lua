--Admob

local ads = require( "ads" )
ads.init( "admob", "ca-app-pub-6355492531112092/3027900561")

function loadAds( ... )
	ads.load( "interstitial", { appId="ca-app-pub-6355492531112092/5326415360" } )
	ads.load( "banner", { x=0, y=10000, appId="ca-app-pub-6355492531112092/3027900561" } )
end

function showBanner()
	ads.show( "banner", { x=0, y=10000, appId="ca-app-pub-6355492531112092/3027900561" } )
end

function hideBanner()
	ads.hide()
end

function showAdPopup()
	ads.show( "interstitial", { appId="ca-app-pub-6355492531112092/5326415360" }  )
end
