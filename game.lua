-- Game

require( "result" )

-- Variables
local balloon_explode_se = audio.loadSound( "Resources/sounds/se_balloon_explode.wav" ) -- SE for balloon explosion
local balloon_collect_se = audio.loadSound( "Resources/sounds/se_balloon_collect.wav" ) -- SE for balloon collection
local balloon_miss_se = audio.loadSound( "Resources/sounds/se_balloon_miss.wav" ) -- SE for balloon miss
local bgm = audio.loadSound( "Resources/sounds/bgm2.wav" ) -- BGM of game
local numLeft = {} -- Array of numbers left 
local countNum = 0 -- Count how many numbers were collected
countNumText=nil  -- Display text for countNum
local countAllNum = 0 -- Count all numbers that were appeared
local countWrong = 0 -- Count Wrong
local countMiss = 0 -- Count Miss
local isPause = false
local balloonGenerator,pauseButton,playButton, groupPause
local stopGame = false
local isEnd = false

-- Function for start a new game
function startGame()
	blockInput = false
	-- Play BGM
	if(isBGM == true) then
		audio.play( bgm , { channel=1, loops=-1 } )
		audio.fade( { channel=1, time=3000, volume=1 } ) 
	end

	-- createBG()
	hideHowtoButton()

	-- Start physics of game
	local physics = require("physics")
	physics.start( )

	-- Add Pause
	if(pauseButton~=nil)then pauseButton:removeSelf( ) end
	pauseButton = display.newImage( "Resources/images/pause.png", display.screenOriginX + display.viewableContentWidth - 80, display.screenOriginY + 80)
	pauseButton:scale( 0.6, 0.6 )
	pauseButton.alpha =0.7

	-- Add Resume
	if(playButton~=nil)then playButton:removeSelf( ) end
	playButton = display.newImage( "Resources/images/play.png", display.screenOriginX + display.viewableContentWidth - 80, display.screenOriginY + 80)
	playButton:scale( 0.6, 0.6 )
	playButton.alpha =0
	
	-- Add Event for Pause Button
	pauseButton:addEventListener( "tap", pauseGame)

	-- Initial Array of numbers left
	for i=1,#numLeft do
		table.remove( numLeft )
	end
	for i=1,maxNum do
		table.insert( numLeft, i)
	end

	-- Initial Count Number
	countWrong = 0
	countMiss = 0
	countAllNum = 0
	countNum = 0 
	if(countNumText~=nil)then countNumText:removeSelf( ) end
	countNumText = display.newText(countNum .. "/" .. maxNum, display.contentWidth/2, display.screenOriginY + 150 , "Resources/fonts/myfont.otf", 128)

	-- Unpause game
	stopGame = false
	isEnd = false
	isPause = false

	-- Start create Balloon
	addNewBalloon()
end

-- Function to remove balloon when touched
function balloonTouched( event )
	if(blockInput == true) then return end
	-- If balloon is not begun or game is pause, do nothing
	if ( event.phase == "began" and isPause == false and isEnd == false ) then

		-- Make the game at Runtime not listen event of 'enterFrame' for this Balloon (which is 'offscreen')
		Runtime:removeEventListener( "enterFrame", event.self )

		-- If the number has not been collected yet, ..
		if (table.indexOf(numLeft,tonumber(event.target[2].text)) ~= nil) then

			-- Remove the number from the list
			table.remove( numLeft, table.indexOf(numLeft,tonumber(event.target[2].text)) )

			--[[local strPrint = ""
			for i=1,#numLeft do
				strPrint = strPrint .. numLeft[i] .. " "
			end
			print(strPrint)--]]

			-- Play Balloon Collect SE
			if(isSE == true) then
				audio.play( balloon_collect_se)
			end

			-- Increase count balloon number
			countNum = countNum + 1
			countNumText.text = countNum .. "/" .. maxNum

			-- End game when finish
			if(countNum == maxNum) then
				isEnd = true
			end

			-- Make it float faster
			event.target.gravityScale=-10

			-- Remove Touch Event
			event.target:removeEventListener( "touch", balloonTouched )
		else

			-- If already collected, explode and remove

			-- Count Wrong
			countWrong = countWrong+1

			-- Show Explode Image
			local explode = display.newImage("Resources/images/explode.png",event.target.x,event.target.y)
			explode.alpha = 0.7

			-- Show Wrong Text
			local wrongText = display.newImage("Resources/images/wrong.png",event.target.x,event.target.y, "Resources/fonts/myfont.otf", 72) 
			wrongText:scale(0.3,0.3)
			wrongText.alpha = 0.7

			-- Play Explode SE
			if(isSE == true) then
				audio.play( balloon_explode_se)
			end

			-- Remove Balloon
			event.target:removeSelf( )

			-- Remove Explode after 400ms
			timer.performWithDelay( 400, function () explode:removeSelf( ) wrongText:removeSelf( ) end )
		end
	end
end

-- Function to remove balloon when off screen
function offscreen( self,event )

	-- If unidentify y, do nothing
	if (self.y == nil) then
		return
	end

	if(isEnd == true) then
		self.gravityScale = -10
	end

	-- Clear Balloon when game stop
	if (stopGame == true) then
		-- Make the game at Runtime not listen event of 'enterFrame' for this Balloon (which is 'offscreen')
		Runtime:removeEventListener( "enterFrame", event.self )
		-- Remove Balloon
		self:removeSelf( )
	end

	-- If y < -150 (above position of -150 from the screen ), ...
	if(self.y < -150) then

		-- If the number has not been collected yet, explode!!
		if (table.indexOf(numLeft,tonumber(self[2].text)) ~= nil) then

			--[[local strPrint = ""
			for i=1,#numLeft do
				strPrint = strPrint .. numLeft[i] .. " "
			end
			print(strPrint)--]]

			-- Count Miss
			countMiss = countMiss+1

			-- Play Miss SE
			if(isSE == true) then
				audio.play( balloon_miss_se)
			end

			-- Show Miss Text
			local missText = display.newImage("Resources/images/miss.png",self.x,display.screenOriginY + 50, "Resources/fonts/myfont.otf", 72) 
			missText:scale(0.3,0.3)
			missText.alpha = 0.7

			-- Remove Miss Text After 400ms
			timer.performWithDelay( 400, function () missText:removeSelf( ) end )
		end

		-- Make the game at Runtime not listen event of 'enterFrame' for this Balloon (which is 'offscreen')
		Runtime:removeEventListener( "enterFrame", event.self )

		-- Remove Balloon
		self:removeSelf( )
	end
end

-- Create balloon
function addNewBalloon( event )
	-- Stop add when game pause


	-- If you collected all numbers
	if (#numLeft == 0) then
		-- Stop the game
		timer.cancel(event.source)

		-- Fade Out BGM
		if(isBGM == true) then
			audio.fade( { channel=1, time=3000, volume=0 } )	
		end

		-- Delete Pause Button
		pauseButton:removeSelf( )
		pauseButton=nil
		playButton:removeSelf( )
		playButton=nil

		-- Show Result
		timer.performWithDelay( 1000, function () showResult(countMiss,countWrong, (1 - (countMiss+countWrong)/countAllNum)*100) end )

		return 
	end
	
	-- Count All Number
	countAllNum = countAllNum+1

	-- Random X-Position to create a balloon
	local startX = math.random( display.screenOriginX + display.viewableContentWidth*0.1,display.screenOriginX + display.viewableContentWidth*0.9)

	-- Create a new Ballon Group (Balloon + Number)
	local balloon_group = display.newGroup()

	-- Set Balloon Group Position
	balloon_group.x = startX
	balloon_group.y = display.screenOriginY + display.viewableContentHeight +150

	-- Create Balloon
	local balloon = display.newImage("Resources/images/balloon".. math.random( 6 ) ..".png",0,0)

	-- Random Number for the list of left numbers
	local numberIndex = math.random( #numLeft )
	local number = numLeft[numberIndex]

	-- For 90%, Random a new Number which can already be collected
	if(math.random(100)>30) then number = math.random( maxNum ) end

	-- Create Number Shadow
	local numberShadow = display.newText( number, 5, -15, "Resources/fonts/myfont.otf", 96) 
	numberShadow:setTextColor( 0.5, 0.5, 0.5, 0.5 )

	-- Create Number Text
	local numberText = display.newText( number, 0, -20, "Resources/fonts/myfont.otf", 96) 
	numberText:setTextColor( 1, 1, 1, 1 )

	-- Insert Balloon and Number into Balloon Group
	balloon_group:insert( balloon )
	balloon_group:insert( numberShadow )
	balloon_group:insert( numberText )

	-- Add Body for Balloon Group
	physics.addBody( balloon_group , "dynamic", { friction=0, bounce=0.8} )

	-- Fix the rotation of Balloon
	balloon_group.isFixedRotation=true

	-- Random Apply Force in X-Axis
	balloon_group:applyForce(math.random(2000)-1000,0,balloon_group.x,balloon_group.y)

	-- Reverse Gravitiy to make it float
	balloon_group.gravityScale = -0.5

	-- Balloon Group will do a function 'offscreen' when it enter the frame
	balloon_group.enterFrame = offscreen

	-- Make the game at Runtime alway listen event of 'enterFrame' for Balloon Group (which is 'offscreen')
	Runtime:addEventListener( "enterFrame", balloon_group )

	-- Balloon Group will do 'balloonTouched' when it is touched
	balloon_group:addEventListener( "touch", balloonTouched )

	-- Add New Balloon
	balloonGenerator = timer.performWithDelay( math.random( 800,2000 ), addNewBalloon)
end

-- Pause Game Function
function pauseGame()
	if(blockInput == true) then return end
	blockInput = true
	-- Prevent double pause
	if (isPause == true) then
		return
	end
	isPause = true
	physics.pause( )
	timer.pause(balloonGenerator)
	pauseButton.alpha=0
	pauseButton:removeEventListener( "tap", pauseGame )
	playButton.alpha=0.7
	timer.performWithDelay(1000,function() playButton:addEventListener( "tap", resumeGame ) end)
	showHowtoButton()
	showPausePopup()
end

-- Resume Game Function
function resumeGame()
	if(blockInput == true) then return end
	-- Prevent double resume
	if (isPause == false) then
		return
	end
	if(groupPause ~= nil) then 
		transition.moveTo( groupPause, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
		timer.performWithDelay( 1000, function () 
			groupPause:removeSelf( ) 
			groupPause=nil
		end )
	end
	isPause = false 
	physics.start( )
	timer.resume(balloonGenerator)
	pauseButton.alpha=0.7
	timer.performWithDelay(1000,function() 
		if (pauseButton ~= nil and isEnd == false) then
			pauseButton:addEventListener( "tap", pauseGame )
		end
	end)
	playButton.alpha=0
	playButton:removeEventListener( "tap", resumeGame )
	hideHowtoButton()
	blockInput=false
end

-- Show pause popup
function showPausePopup()

	-- Create Group and set under display
	groupPause = display.newGroup( )
	groupPause.x = display.contentWidth*0.5
	groupPause.y = display.contentHeight*3

	-- Create Board
	local pBoard = display.newRoundedRect(groupPause, 0, 0, display.viewableContentWidth*0.8 , display.viewableContentHeight*0.6 , 50)
	pBoard:setFillColor( 0 , 0.7, 0.8, 0.6 )

	-- Create Restart Button
	local restartPButton = display.newImage(groupPause, "Resources/images/replay.png",pBoard.width/2*0.33, (pBoard.height/2)*0.8)
	restartPButton:scale( 0.8, 0.8 )

	-- Add Event for Reset Button
	restartPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		blockInput = true
		-- Move Result Popup down
		stopGame=true
		pauseButton:removeSelf( )
		pauseButton=nil
		playButton:removeSelf( )
		playButton=nil
		countNumText:removeSelf( )
		countNumText=nil
		--playButton:removeEventListener( "tap", resumeGame )
		transition.moveTo( groupPause, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
		audio.fade( { channel=1, time=1000, volume=0 } ) 
		timer.performWithDelay( 1000, function () 
			-- Prevent double restart
			if(groupPause == nil) then 
				return
			end
			-- Stop the game
			timer.cancel(balloonGenerator)
			-- Remove Result Popup
			groupPause:removeSelf( ) 
			groupPause=nil
			-- Start Game
			timer.performWithDelay(1000,startGame) 
		end ) 
	end )

	-- Create Menu Button
	local menuPButton = display.newImage(groupPause, "Resources/images/menu.png",-pBoard.width/2*0.33, (pBoard.height/2)*0.8)
	menuPButton:scale( 0.8, 0.8 )

	-- Add Event for Menu Button
	menuPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		blockInput = true
		-- Move Result Popup down
		stopGame=true
		pauseButton:removeSelf( )
		pauseButton=nil
		playButton:removeSelf( )
		playButton=nil
		countNumText:removeSelf( )
		countNumText=nil
		--playButton:removeEventListener( "tap", resumeGame )
		transition.moveTo( groupPause, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
		audio.fade( { channel=1, time=1000, volume=0 } ) 
		timer.performWithDelay( 1000, function () 
			-- Prevent double restart
			if(groupPause == nil) then 
				return
			end
			-- Stop the game
			timer.cancel(balloonGenerator)
			-- Remove Result Popup
			groupPause:removeSelf( ) 
			groupPause=nil
			-- Start Game

			showAdPopup()
			timer.performWithDelay( 500,mainPopup)
		end ) 
	end )

	-- Create Miss Text
	local missPText = display.newImage(groupPause, "Resources/images/miss_result.png",-pBoard.width/2*0.5 ,-pBoard.height/2 *0.6+15) 
	missPText:scale(0.3,0.3)
	local missCountPText = display.newText(groupPause, countMiss ,pBoard.width/2*0.5 ,-pBoard.height/2 *0.6, "Resources/fonts/myfont.otf", 128) 
	missCountPText:setTextColor( 1, 1, 1, 1 )
	
	-- Create Wrong Text
	local wrongPText = display.newImage(groupPause, "Resources/images/wrong_result.png",-pBoard.width/2*0.5 ,-pBoard.height/2 *0.2+20) 
	wrongPText:scale(0.3,0.3)
	local wrongCountPText = display.newText(groupPause, countWrong ,pBoard.width/2*0.5 ,-pBoard.height/2 *0.2, "Resources/fonts/myfont.otf", 128) 
	wrongCountPText:setTextColor( 1, 1, 1, 1 )

	-- Create BGM Mute Button
	local bgmOffPButton = display.newImage(groupPause, "Resources/images/bgm_off.png",-pBoard.width/2*0.33, (pBoard.height/2)*0.3)

	-- Create BGM UnMute Button
	local bgmOnPButton = display.newImage(groupPause, "Resources/images/bgm_on.png",-pBoard.width/2*0.33, (pBoard.height/2)*0.3)

	if (isBGM == true) then
		bgmOffPButton.alpha = 0
	else
		bgmOnPButton.alpha = 0
	end

	bgmOnPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isBGM == false or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isBGM = false end)
		audio.stop( {channel=1} )
		saveData("BGM","false")
		bgmOnPButton.alpha = 0
		bgmOffPButton.alpha = 1
	end )
	bgmOffPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isBGM == true or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isBGM = true end)
		audio.play( bgm , { channel=1, loops=-1 } )
		saveData("BGM","true")
		bgmOffPButton.alpha = 0
		bgmOnPButton.alpha = 1
	end )

	-- Create SE Mute Button
	local seOffPButton = display.newImage(groupPause, "Resources/images/se_off.png",pBoard.width/2*0.33, (pBoard.height/2)*0.3)

	-- Create SE UnMute Button
	local seOnPButton = display.newImage(groupPause, "Resources/images/se_on.png",pBoard.width/2*0.33, (pBoard.height/2)*0.3)

	if (isSE == true) then
		seOffPButton.alpha = 0
	else
		seOnPButton.alpha = 0
	end

	seOnPButton:addEventListener( "tap", function ()
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isSE == false or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isSE = false end)
		saveData("SE","false")
		seOnPButton.alpha = 0
		seOffPButton.alpha = 1
	end )
	seOffPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isSE == true or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isSE = true end)
		saveData("SE","true")
		seOffPButton.alpha = 0
		seOnPButton.alpha = 1
	end )

	-- Move Board to Center
	transition.moveTo( groupPause, { x=display.contentWidth*0.5, y=display.contentHeight*0.5, time=1000 } )
	timer.performWithDelay(1000, function ( )
		blockInput=false
	end)

end