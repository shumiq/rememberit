-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

require("admob")
require("ga")
require("splash")
require("game")
require("save")
require("howto")

loadAds()

-- Global Variables
isBGM=false
isSE=false
maxNum = 10 -- Limit of Number
balloonLeft = 0
isStart = false
blockInput = false
bkg = nil
cloudTimer = nil

-- Show Splash Screen
showSplash()

-- Wall
leftWall = nil
rightWall = nil
groupMain = nil

-- Function which will execute after splash screen
function afterSplash( )

	-- Add background
	createBG()

	-- Create Save data is not existed
	if(loadData("BGM") == "") then saveData("BGM","true") end
	if(loadData("SE") == "") then saveData("SE","true") end
	if(loadData("LEVEL") == "") then saveData("LEVEL","10") end

	-- Load Save Data
	isBGM = loadData("BGM") == "true"
	isSE = loadData("SE") == "true"
	maxNum = tonumber(loadData("LEVEL"))

	-- Show AdMob Banner
	showBanner()

	-- Show Main Popup
	mainPopup()
end

function createBG()
	bkg = display.newGroup()
	bkg.x = display.contentWidth*0.5
	bkg.y = display.contentHeight*0.5
	local filter = display.newRect(bkg, 0,0, display.viewableContentWidth, display.viewableContentHeight)
	if(cloudTimer~=nil) then timer.cancel(cloudTimer) end
	local currentHour = tonumber(os.date("%H"))
	if (currentHour >= 19 or currentHour <= 5) then
		-- Night
		paint = {
		    type = "gradient",
		    color1 = { 0,0,0.1 },
		    color2 = {  0,0.1,0.3 },
		    direction = "down"
		}
		filter.fill = paint
		addCloud(0,0.1)  --]]
	elseif (currentHour >= 6 and currentHour <= 10) then
		-- Morning
		paint = {
		    type = "gradient",
		    color1 = { 0,0.87,1 },
		    color2 = {  0.5,1,1 },
		    direction = "down"
		}
		filter.fill = paint
		addCloud(0,0.4)  --]]
	elseif (currentHour >= 11 and currentHour <= 14) then
		-- Afternoon
		paint = {
		    type = "gradient",
		    color1 = { 0,0.7,1,1 },
		    color2 = {  0,0.87,1 },
		    direction = "down"
		}
		filter.fill = paint
		addCloud(0,0.2) --]]
	elseif (currentHour >= 15 and currentHour <= 16) then
		-- Evening
		local paint = {
		    type = "gradient",
		    color1 = { 0,0.7,0.7,1 },
		    color2 = {  1,0.9,0.5  },
		    direction = "down"
		}
		filter.fill = paint
		addCloud(0,0.2) --]]
	elseif (currentHour >= 17 and currentHour <= 18) then
		-- Twilight
		local paint = {
		    type = "gradient",
		    color1 = { 0.4,0.2,0.5,1 },
		    color2 = { 1,0.9,0.5 },
		    direction = "down"
		}
		filter.fill = paint
		addCloud(0,0.2) --]]
	end
end

function addCloud(lastY,cloud_alpha)
	local yAxis = math.random( display.viewableContentHeight*1.2 ) - display.viewableContentHeight*1.2/2
	while (math.abs(lastY-yAxis) < display.viewableContentHeight*0.2) do
		yAxis = math.random( display.viewableContentHeight*1.2 ) - display.viewableContentHeight*1.2/2
	end
	local cloud = display.newImage( bkg, "Resources/images/cloud.png", display.viewableContentWidth, yAxis)
	cloud.alpha = cloud_alpha
	local ratio = display.viewableContentWidth/cloud.contentWidth * math.random( 5,8 ) / 10.0
	cloud:scale( ratio,ratio )
	transition.moveTo( cloud, { x=-display.viewableContentWidth*2, y=yAxis, time=math.random( 20000,30000 ) } )
	timer.performWithDelay( 30000, function() cloud:removeSelf( ) end )
	cloudTimer = timer.performWithDelay( math.random( 3000,8000 ), function() addCloud(yAxis,cloud_alpha) end )
end

-- Function to remove balloon when off screen
function offscreenForTitle( self,event )
	-- If unidentify y, do nothing
	if (self.y == nil) then
		return
	end
	if (isStart == true) then
		self.gravityScale = -10
	end
	-- If y < -150 (above position of -150 from the screen ), ...
	if(self.y < -150) then
		balloonLeft = balloonLeft -1
		-- Make the game at Runtime not listen event of 'enterFrame' for this Balloon (which is 'offscreen')
		Runtime:removeEventListener( "enterFrame", event.self )
		-- Remove Balloon
		self:removeSelf( )
	end
end

-- Create balloon
function addNewBalloonForTitle( event )

	-- Random X-Position to create a balloon
	local startX = math.random( display.screenOriginX + display.viewableContentWidth*0.1,display.screenOriginX + display.viewableContentWidth*0.9)

	-- Create a new Ballon Group (Balloon + Number)
	local balloon_group = display.newGroup()

	-- Set Balloon Group Position
	balloon_group.x = startX
	balloon_group.y = display.screenOriginY + display.viewableContentHeight +150

	-- Create Balloon
	local balloon = display.newImage("Resources/images/balloon".. math.random( 6 ) ..".png",0,0)

	-- Random a new Number which can already be collected
	number = math.random( 100 )

	-- Create Number Shadow
	local numberShadow = display.newText( number, 5, -15, "Resources/fonts/myfont.otf", 96) 
	numberShadow:setTextColor( 0.5, 0.5, 0.5, 0.5 )

	-- Create Number Text
	local numberText = display.newText( number, 0, -20, "Resources/fonts/myfont.otf", 96) 
	numberText:setTextColor( 1, 1, 1, 1 )

	-- Insert Balloon and Number into Balloon Group
	balloon_group:insert( balloon )
	balloon_group:insert( numberShadow )
	balloon_group:insert( numberText )

	-- Add Body for Balloon Group
	physics.addBody( balloon_group , "dynamic", { friction=0, bounce=0.8} )

	-- Fix the rotation of Balloon
	balloon_group.isFixedRotation=true

	-- Random Apply Force in X-Axis
	balloon_group:applyForce(math.random(2000)-1000,0,balloon_group.x,balloon_group.y)

	-- Reverse Gravitiy to make it float
	balloon_group.gravityScale = -0.5

	-- Balloon Group will do a function 'offscreen' when it enter the frame
	balloon_group.enterFrame = offscreenForTitle

	-- To Back
	balloon_group:toBack( )
	bkg:toBack( )

	-- Make the game at Runtime alway listen event of 'enterFrame' for Balloon Group (which is 'offscreen')
	Runtime:addEventListener( "enterFrame", balloon_group )

	balloonLeft = balloonLeft + 1
end

function mainPopup()
	blockInput = false
	local bgm = audio.loadSound( "Resources/sounds/bgm2.wav" ) -- BGM of game
	-- Play BGM
	if(isBGM == true) then
		audio.play( bgm , { channel=1, loops=-1 } )
		audio.fade( { channel=1, time=3000, volume=1 } ) 
	end

	isStart = false

	local physics = require("physics")
	physics.start( )
	-- Add Left & Right Wall
	leftWall = display.newRect(display.screenOriginX-10, 0, 1, display.contentHeight*3+300)
	physics.addBody(leftWall, "static")
	rightWall = display.newRect(display.screenOriginX + display.viewableContentWidth+10, 0, 1, display.contentHeight*3+300)
	physics.addBody(rightWall, "static")

	-- Create Group and set under display
	groupMain = display.newGroup( )
	groupMain.x = display.contentWidth*0.5
	groupMain.y = display.contentHeight*3

	-- Create Board
	local mBoard = display.newRoundedRect(groupMain, 0, 0, display.viewableContentWidth*0.8 , display.viewableContentHeight*0.6 , 50)
	mBoard:setFillColor( 0 , 0.7, 0.8, 0.6 )

	-- Create Title
	local title = display.newImage(groupMain, "Resources/images/title.png",0, -(mBoard.height/2)*0.7)
	title:scale( 0.5, 0.5 )

	-- Create Title
	local start = display.newImage(groupMain, "Resources/images/start.png",0, -(mBoard.height/2)*0.3)
	start:scale( 1, 1 )
	local start_shadow = display.newImage(groupMain, "Resources/images/start_shadow.png",0, -(mBoard.height/2)*0.3)
	start_shadow:scale( 1, 1 )
	start_shadow.alpha = 0
	start_blink = timer.performWithDelay( 2000, function ()
		transition.fadeIn(start_shadow,{time=1000})
		timer.performWithDelay(1000,function() 
			transition.fadeOut(start_shadow,{time=1000})
		end)
	end, 0)

	-- Create Level  Button
	local easyButton = display.newImage(groupMain, "Resources/images/easy.png",-mBoard.width/2*0.66, (mBoard.height/2)*0.2)
	easyButton:scale( 0.6,0.6 )
	local normalButton = display.newImage(groupMain, "Resources/images/normal.png",0, (mBoard.height/2)*0.2)
	normalButton:scale( 0.6,0.6 )
	local hardButton = display.newImage(groupMain, "Resources/images/hard.png",mBoard.width/2*0.66, (mBoard.height/2)*0.2)
	hardButton:scale( 0.6,0.6 )
	local easyButtonSelected = display.newImage(groupMain, "Resources/images/easy_selected.png",-mBoard.width/2*0.66, (mBoard.height/2)*0.2)
	easyButtonSelected:scale( 0.6,0.6 )
	easyButtonSelected.alpha = 0
	local normalButtonSelected = display.newImage(groupMain, "Resources/images/normal_selected.png",0, (mBoard.height/2)*0.2)
	normalButtonSelected:scale( 0.6,0.6 )
	normalButtonSelected.alpha = 0
	local hardButtonSelected = display.newImage(groupMain, "Resources/images/hard_selected.png",mBoard.width/2*0.66, (mBoard.height/2)*0.2)
	hardButtonSelected:scale( 0.6,0.6 )
	hardButtonSelected.alpha = 0

	if(maxNum == 10) then 
		easyButtonSelected.alpha=1
	end 
	if(maxNum == 30) then 
		normalButtonSelected.alpha=1
	end 
	if(maxNum == 100) then 
		hardButtonSelected.alpha=1
	end 

	easyButton:addEventListener("tap" , function ()
		if(blockInput == true) then return end
		maxNum = 10
		saveData("LEVEL","10")
		easyButtonSelected.alpha=1
		normalButtonSelected.alpha=0
		hardButtonSelected.alpha=0
	end)
	normalButton:addEventListener("tap" , function ()
		if(blockInput == true) then return end
		maxNum = 30
		saveData("LEVEL","30")
		easyButtonSelected.alpha=0
		normalButtonSelected.alpha=1
		hardButtonSelected.alpha=0
	end)
	hardButton:addEventListener("tap" , function ()
		if(blockInput == true) then return end
		maxNum = 100
		saveData("LEVEL","100")
		easyButtonSelected.alpha=0
		normalButtonSelected.alpha=0
		hardButtonSelected.alpha=1
	end)

	-- Create BGM Mute Button
	local bgmOffPButton = display.newImage(groupMain, "Resources/images/bgm_off.png",-mBoard.width/2*0.33, (mBoard.height/2)*0.7)

	-- Create BGM UnMute Button
	local bgmOnPButton = display.newImage(groupMain, "Resources/images/bgm_on.png",-mBoard.width/2*0.33, (mBoard.height/2)*0.7)

	if (isBGM == true) then
		bgmOffPButton.alpha = 0
	else
		bgmOnPButton.alpha = 0
	end

	bgmOnPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isBGM == false or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isBGM = false end)
		audio.stop( {channel=1} )
		saveData("BGM","false")
		bgmOnPButton.alpha = 0
		bgmOffPButton.alpha = 1
	end )
	bgmOffPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isBGM == true or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isBGM = true end)
		audio.play( bgm , { channel=1, loops=-1 } )
		saveData("BGM","true")
		bgmOffPButton.alpha = 0
		bgmOnPButton.alpha = 1
	end )

	-- Create SE Mute Button
	local seOffPButton = display.newImage(groupMain, "Resources/images/se_off.png",mBoard.width/2*0.33, (mBoard.height/2)*0.7)

	-- Create SE UnMute Button
	local seOnPButton = display.newImage(groupMain, "Resources/images/se_on.png",mBoard.width/2*0.33, (mBoard.height/2)*0.7)

	if (isSE == true) then
		seOffPButton.alpha = 0
	else
		seOnPButton.alpha = 0
	end

	seOnPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isSE == false or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isSE = false end)
		saveData("SE","false")
		seOnPButton.alpha = 0
		seOffPButton.alpha = 1
	end )
	seOffPButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		-- Prevent Double Touch
		if(isSE == true or stopGame==true) then
			return
		end
		timer.performWithDelay(500, function() isSE = true end)
		saveData("SE","true")
		seOffPButton.alpha = 0
		seOnPButton.alpha = 1
	end )

	showHowtoButton()

	-- Move Board to Center
	transition.moveTo( groupMain, { x=display.contentWidth*0.5, y=display.contentHeight*0.5, time=1000 } )

	local balloonTimer = timer.performWithDelay(1000,addNewBalloonForTitle,0)

	start:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		blockInput = true
		isStart = true
		transition.moveTo( groupMain, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
		timer.cancel(balloonTimer)
		-- Fade Out BGM
		if(isBGM == true) then
			audio.fade( { channel=1, time=3000, volume=0 } )	
		end
		timer.performWithDelay(1000,function (event)
			if(balloonLeft==0 and groupMain~=nil) then
				groupMain:removeSelf( )
				groupMain = nil
				startGame()
				timer.cancel(event.source)
			end
		end, 0 )
	end )

end 

-- After Show Splash screen..
timer.performWithDelay( 4000, afterSplash)