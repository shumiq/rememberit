-- Load Save Data
function loadData(data)

	-- Path for the file to read
	local path = system.pathForFile( data .. ".txt", system.DocumentsDirectory )
	 
	-- Open the file handle
	local file = io.open( path, "r" )
	 
	if not file then
	    -- return empty string if there is no file
	    return ""
	else
	    -- Read data from file
	    local contents = file:read( "*a" )
		-- Close the file handle
		io.close( file )
	    -- Output the file contents
	    return contents
	end

	file = nil
end

-- Save Data
function saveData(data, value)

	-- Path for the file to write
	local path = system.pathForFile(data .. ".txt", system.DocumentsDirectory )
	 
	-- Open the file handle
	local file = io.open( path, "w" )
	 
	if file then
	    -- Write data to file
	    file:write( value )
		-- Close the file handle
		io.close( file )
	end
 
	file = nil
end