local howtoButton = display.newImage( "Resources/images/howto.png", 80, display.screenOriginY + 80)
howtoButton:scale( 0.6, 0.6 )
howtoButton.alpha=0
local howtoGroup

function showHowtoButton()
	howtoButton.alpha=0.7
	howtoButton:toFront( )
	howtoButton:addEventListener( "tap", showHowtoPopup )
end

function hideHowtoButton()
	howtoButton.alpha=0
	howtoButton:removeEventListener( "tap", showHowtoPopup )
end

function showHowtoPopup()
	if(blockInput==true) then return end
	blockInput = true

	-- Create Title
	local help = display.newImage("Resources/images/howtoplay.png",display.contentWidth*0.5, display.contentHeight*3)
	local ratio = display.viewableContentWidth/help.contentWidth
	help:scale( ratio, ratio )
	help:toFront( )

	-- Move Board to Center
	transition.moveTo( help, { x=display.contentWidth*0.5, y=display.contentHeight*0.5, time=1000 } )

	local isClose = false
	timer.performWithDelay( 1000, function() 
		help:addEventListener("tap", function( )
			print("close help")
			if(isClose==nil)then return end
			isClose=true
			transition.moveTo( help, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
			timer.performWithDelay( 1000, function(  )
				help:removeSelf( )
				help=nil
				blockInput=false
			end)
		end)
	end)
end