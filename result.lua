-- result

local tick_se = audio.loadSound( "Resources/sounds/se_tick.wav" ) -- SE for balloon miss

-- Instatiate Variables
local missCount, missCountText, wrongCount, wrongCountText,restartButton,menuButton,shareButton,groupResult,rankText,rankCaption,board,rankShadow
local realMiss, realWrong, realPercent, screenshot
local stillCapture = false

-- Show Result Popup
function showResult(cm, cw, p)

	-- Receive Miss, Wrong, Percent from game
	realMiss=cm
	realWrong=cw
	realPercent=math.floor(p)

	-- Create Group and set under display
	groupResult = display.newGroup( )
	groupResult.x = display.contentWidth*0.5
	groupResult.y = display.contentHeight*3

	-- Create Board
	board = display.newRoundedRect(groupResult, 0, 0, display.viewableContentWidth*0.8 , display.viewableContentHeight*0.6 , 50)
	board:setFillColor( 0 , 0.7, 0.8, 0.6 )

	-- Create Restart Button
	restartButton = display.newImage(groupResult, "Resources/images/replay.png", 0, (board.height/2)*0.8)
	restartButton:scale( 0.8, 0.8 )
	restartButton.alpha = 0

	-- Create Menu Button
	menuButton = display.newImage(groupResult, "Resources/images/menu.png",-board.width/2*0.66, (board.height/2)*0.8)
	menuButton:scale( 0.8, 0.8 )
	menuButton.alpha = 0

	-- Create Restart Button
	shareButton = display.newImage(groupResult, "Resources/images/share.png", board.width/2*0.66, (board.height/2)*0.8)
	shareButton:scale( 0.8, 0.8 )
	shareButton.alpha = 0

	-- Create Miss Text
	missText = display.newImage(groupResult, "Resources/images/miss_result.png",-board.width/2*0.5 ,board.height/2 *0.2+15) 
	missText:scale(0.3,0.3)
	missCount = 0
	missCountText = display.newText(groupResult, missCount ,board.width/2*0.5 ,board.height/2 *0.2, "Resources/fonts/myfont.otf", 128) 
	missCountText:setTextColor( 1, 1, 1, 1 )
	
	-- Create Wrong Text
	wrongText = display.newImage(groupResult, "Resources/images/wrong_result.png",-board.width/2*0.5 ,board.height/2 *0.5+20) 
	wrongText:scale(0.3,0.3)
	wrongCount = 0
	wrongCountText = display.newText(groupResult, wrongCount ,board.width/2*0.5 ,board.height/2 *0.5, "Resources/fonts/myfont.otf", 128) 
	wrongCountText:setTextColor( 1, 1, 1, 1 )

	-- Create Percent Text
	percentCount = 0
	percentCountText = display.newText(groupResult, percentCount .. "%" ,board.width/2*0.5 ,-board.height/2 *0.5, "Resources/fonts/myfont.otf", 150) 
	percentCountText:setTextColor( 1, 1, 1, 1 )
	percentCountText.alpha = 0

	-- Move Board to Center
	transition.moveTo( groupResult, { x=display.contentWidth*0.5, y=display.contentHeight*0.5, time=1000 } )

	-- Tick Miss Count
	timer.performWithDelay( 1000, tickMiss)
end

-- Tick Miss Count
function tickMiss()

	-- Add Event to Skip Tick
	missCountText:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		missCount=realMiss
	end )

	-- If still Not tick to target, Tick
	if(missCount<realMiss) then
   		timer.performWithDelay( 100, function ()
   			missCount = missCount+1
   			missCountText.text = missCount
			if(isSE == true) then
   				audio.play( tick_se )
   			end
   			tickMiss()
   		end )
   	else
   		-- If Complete, remove event and Tick Wrong Count
		missCountText:removeEventListener( "tap", function () 
			if(blockInput == true) then return end
			missCount=realMiss
		end )
   		timer.performWithDelay( 1000, tickWrong)
   	end
end

-- Tick Miss Count
function tickWrong()

	-- Add Event to Skip Tick
	wrongCountText:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		wrongCount=realWrong
	end )

	-- If still Not tick to target, Tick
	if(wrongCount<realWrong) then
   		timer.performWithDelay( 100, function ()
   			wrongCount = wrongCount+1
   			wrongCountText.text = wrongCount
			if(isSE == true) then
   				audio.play( tick_se )
   			end
   			tickWrong()
   		end )
   	else
   		-- If Complete, remove event and Tick Percent Count
		wrongCountText:removeEventListener( "tap", function () 
			if(blockInput == true) then return end
			wrongCount=realWrong
		end )
   		transition.fadeIn( percentCountText, { time=1000 } ) -- Fadein Percent Text
   		timer.performWithDelay( 2000, tickPercent)
   	end
end

-- Tick Miss Count
function tickPercent()

	-- Add Event to Skip Tick
	percentCountText:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		percentCount=math.floor(realPercent)
   		percentCountText.text = math.min(percentCount,100) .. "%"
	end )

	-- If still Not tick to target, Tick
	if(percentCount<realPercent) then
   		timer.performWithDelay( 10, function ()
   			percentCount = percentCount+1
   			percentCountText.text = math.min(percentCount,100) .. "%"
			if(isSE == true and percentCount%10 == 0) then
   				audio.play( tick_se )
   			end
   			tickPercent()
   		end )
   	else
   		-- If Complete, remove event and Show Rank
   		percentCountText:removeEventListener( "tap", function () 
			if(blockInput == true) then return end
			percentCount=realPercent
		end )
   		timer.performWithDelay( 1000, showRank)
   	end
end

-- Show Rank
function showRank()
	blockInput = true
	local prefix = ""

	-- Set Rank, Shadow Color, and Start Blink Caption
	if(percentCount>=100) then
		prefix = "s"
	elseif (percentCount>90) then
		prefix = "a"
	elseif (percentCount>80) then
		prefix = "b"
	elseif (percentCount>65) then
		prefix = "c"
	elseif (percentCount>50) then
		prefix = "d"
	else
		prefix = "f"
	end

	-- Create Rank & Shadow
	rankText = display.newImage(groupResult, "Resources/images/ranks/"..prefix..".png",-board.width/2*0.5 ,-board.height/2 *0.6)
	rankText.alpha = 0
	rankText:scale( 1.5, 1.5 )
	rankShadow = display.newImage(groupResult, "Resources/images/ranks/"..prefix.."_shadow.png",-board.width/2*0.5 ,-board.height/2 *0.6) 
	rankShadow.alpha = 0
	rankShadow:scale( 1.5, 1.5 )

	-- Fadein Rank & Shadon
	transition.fadeIn(rankText,{time=500})
	transition.fadeIn(rankShadow,{time=500})

	-- Blink Caption
	blinkCaption(prefix,1)
   	
   	-- Fadein Buttons
   	transition.fadeIn( restartButton, { time=1000 } )
   	transition.fadeIn( menuButton, { time=1000 } )
   	transition.fadeIn( shareButton, { time=1000 } )
   	
	timer.performWithDelay(1000,function() 
		showAdPopup()
		blockInput = false
	end)

   	-- Add Event for Reset Button
	restartButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		blockInput = true
		countNumText:removeSelf( )
		countNumText=nil
		-- Move Result Popup down
		transition.moveTo( groupResult, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )

		timer.performWithDelay( 1000, function () 

			-- Prevent double restart
			if(groupResult == nil) then 
				return
			end

			-- Remove Result Popup
			groupResult:removeSelf( ) 
			groupResult=nil
			
			-- Start BGM again
			if(isBGM == true) then
				audio.fade( { channel=1, time=3000, volume=1 } ) 
			end

			-- Start Game
			startGame() 
		end )
	end )

	-- Add Event for Menu Button
	menuButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		blockInput = true
		countNumText:removeSelf( )
		countNumText=nil
		transition.moveTo( groupResult, { x=display.contentWidth*0.5, y=display.contentHeight*3, time=1000 } )
		timer.performWithDelay( 1000, function () 
			-- Prevent double restart
			if(groupResult == nil) then 
				return
			end
			-- Remove Result Popup
			groupResult:removeSelf( ) 
			groupResult=nil
			-- Start Game
			mainPopup()
		end ) 
	end )

	-- Add Event for Share Button
	shareButton:addEventListener( "tap", function () 
		if(blockInput == true) then return end
		if(blockInput == true) then return end
		if(stillCapture == true or rankCaption == nil) then return end
		stillCapture = true
		rankCaption.alpha=1.0
		rankShadow.alpha=1.0
		local banner = display.newImage("Resources/images/banner.png",display.contentWidth/2,display.screenOriginY + display.viewableContentHeight - 128)
		local ss = display.captureScreen( false )
		display.save( ss, { filename="screenshot.jpg", baseDir=system.DocumentsDirectory, isFullResolution=true} )
		local options = {}
		options.image = {
            { filename = "screenshot.jpg", baseDir = system.DocumentsDirectory },
         }
		ss:removeSelf()
		banner:removeSelf( )

		timer.performWithDelay( 1000, function () 
			native.showPopup( "social", options)
			blockInput = false
		end )
		timer.performWithDelay( 3000, function () stillCapture = false end )
	end )
end

-- Blink Caption
function blinkCaption( prefix, mode )

	timer.performWithDelay( 2000, function ()

		-- Prevent Clear Result Popup
		if(groupResult == nil) then 
			return
		end

		-- Switch Text
		if(mode==1) then
			rankCaption = display.newImage(groupResult, "Resources/images/ranks/"..prefix.."_text.png",0,-board.height/2 *0.1) 
			mode=2
		else
			rankCaption = display.newImage(groupResult, "Resources/images/ranks/"..prefix.."_caption.png",0,-board.height/2 *0.1)
			mode=1 
		end
		rankCaption:scale(0.5,0.5)
		rankCaption.alpha =0

		-- Fadein-out
		transition.fadeIn(rankCaption,{time=1000})
		transition.fadeIn(rankShadow,{time=1000})
		timer.performWithDelay(1000,function() 
			transition.fadeOut(rankCaption,{time=1000})
			transition.fadeOut(rankShadow,{time=1000})
		end)

		-- If Result Popup is removed, stop Blink
		if(groupResult ~= nil) then 
			blinkCaption(prefix,mode)
		else
			rankCaption.alpha=0
		end
	end)
end